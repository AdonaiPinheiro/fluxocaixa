import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

export default class HistoricoItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            tipo:'',
            bg:'#FFF'
        };

        if(this.props.data.type == 'receita') {
            this.state.tipo = 'Receita';
            this.state.bg = '#82FA58';
        } else {
            this.state.tipo = 'Despesa';
            this.state.bg = '#F78181';
        }

    }

    render() {

        return(

            <View style={[styles.container, { backgroundColor:this.state.bg }]}>
                <Text style={styles.text}>{this.state.tipo}</Text>
                <Text style={[styles.text, { fontSize:18 }]}>R$ {this.props.data.valor}</Text>
            </View>

        );

    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'row',
        margin:10,
        borderWidth:0.3,
        borderRadius:5,
        padding:10,
        justifyContent:'space-between'
    },
    text:{
        fontSize:16,
        fontWeight:'bold'
    }
});