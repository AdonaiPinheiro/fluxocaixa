import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableHighlight } from 'react-native';
import firebase from '../FirebaseConnection';
import Icon from 'react-native-vector-icons/FontAwesome5'

import HistoricoItem from '../../assets/components/HistoricoItem'

export default class Interna extends Component {

    constructor(props) {
        super(props);
        this.state = {
            saldo:0,
            historico:[]
        };

        this.addReceita = this.addReceita.bind(this);
        this.addDespesa = this.addDespesa.bind(this);
        this.logout = this.logout.bind(this);

        firebase.auth().onAuthStateChanged((user)=>{
            if (user) {

                firebase.database().ref('users').child(user.uid).on('value', (snapshot) => {
                    let state = this.state;
                    state.saldo = snapshot.val().saldo;
                    this.setState(state);

                });

                firebase.database().ref('historico').child(user.uid).on('value', (snapshot)=>{
                    let state = this.state;
                    state.historico = [];
                    snapshot.forEach((childItem)=>{
                        state.historico.push({
                            key:childItem.key,
                            type:childItem.val().type,
                            valor:childItem.val().valor
                        });
                    });
                    this.setState(state);
                });

            } else {
                this.props.navigation.navigate('Home');
            }
        });
    }

    addReceita() {
        this.props.navigation.navigate('AddReceita');
    }

    addDespesa() {
        this.props.navigation.navigate('AddDespesas');
    }

    logout() {
        firebase.auth().signOut();
    }

    render() {
        return(
            <View style={styles.container}>
                <View style={styles.saldoArea}>
                    <Text style={styles.saldo}>Saldo: R$ {this.state.saldo}</Text>
                </View>

                <FlatList
                    style={styles.historico}
                    data={this.state.historico}
                    renderItem={({item})=><HistoricoItem data={item} />}
                />

                <View style={styles.buttonArea}>

                    <TouchableHighlight underlayColor="#CCC" style={styles.button} onPress={this.addReceita}>
                        <Text style={styles.buttonTitle}>Adicionar Receita</Text>
                    </TouchableHighlight>

                    <TouchableHighlight underlayColor="#CCC" style={styles.button} onPress={this.addDespesa}>
                        <Text style={styles.buttonTitle}>Adicionar Despesa</Text>
                    </TouchableHighlight>

                    <TouchableHighlight underlayColor="#CCC" style={[styles.button, { backgroundColor:'transparent' }]} onPress={this.logout}>
                        <Icon name="sign-out-alt" size={36} solid />
                    </TouchableHighlight>

                </View>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container:{
        flex:1
    },
    saldoArea:{
        paddingTop:20,
        paddingBottom:20,
        backgroundColor:'#EEE'
    },
    saldo:{
        textAlign:'center',
        fontSize:18
    },
    historico:{
        flex:1
    },
    buttonArea:{
        flexDirection:'row',
        padding:10,
        backgroundColor:'#DDD',
        justifyContent:'space-around',
        alignItems:'center'
    },
    button:{
        paddingLeft:10,
        paddingRight:10,
        margin:5,
        height:40,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#FFF',
        borderRadius:5
    },
    buttonTitle:{
        fontSize:16
    }
});