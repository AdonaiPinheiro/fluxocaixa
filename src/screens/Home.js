import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, TouchableHighlight, StatusBar } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {};

        this.cadastrar = this.cadastrar.bind(this);
        this.login = this.login.bind(this);
    }

    login() {
        this.props.navigation.navigate('Login');
    }

    cadastrar() {
        this.props.navigation.navigate('Cadastro');
    }

    render() {
        return(
            <ImageBackground source={require('../../assets/images/back.jpg')} style={styles.bg}>
                <View style={styles.container}>
                <StatusBar hidden={true} />
                    <Icon name="money-bill-wave" size={72} color="#FFF" />
                    <Text style={styles.title}>Fluxo de Caixa</Text>

                    <View style={styles.buttonArea}>

                        <TouchableHighlight underlayColor="#CCC" style={styles.button} onPress={this.cadastrar}>
                            <Text style={styles.buttonTitle}>Cadastrar</Text>
                        </TouchableHighlight>

                        <TouchableHighlight underlayColor="#CCC" style={styles.button} onPress={this.login}>
                            <Text style={styles.buttonTitle}>Login</Text>
                        </TouchableHighlight>

                    </View>

                </View>
            </ImageBackground>
        );
    }

}

const styles = StyleSheet.create({
    bg:{
        flex:1,
        width:null
    },
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    title:{
        fontSize:30,
        backgroundColor:'transparent',
        color:'#FFF',
        marginTop:10
    },
    buttonArea:{
        marginTop:10,
        flexDirection:'row'
    },
    button:{
        margin:10,
        height:40,
        width:100,
        backgroundColor:'#FFF',
        alignItems:'center',
        justifyContent:'center',
        borderRadius:5
    },
    buttonTitle:{
        fontSize:16
    }
});