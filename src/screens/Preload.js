import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, StatusBar } from 'react-native';
import firebase from '../FirebaseConnection';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default class Preload extends Component {

    constructor(props) {
        super(props);
        this.state = {};

        firebase.auth().onAuthStateChanged((user)=>{
            if (user) {
                this.props.navigation.navigate('Interna')
            } else {
                this.props.navigation.navigate('Home');
            }
        });
    }

    render() {
        return(
            <ImageBackground source={require('../../assets/images/back.jpg')} style={styles.bg}>
                <View style={styles.container}>
                <StatusBar hidden={true} />
                    <Icon name="money-bill-wave" size={72} color="#FFF" />
                    <Text style={styles.title}>Fluxo de Caixa</Text>
                </View>
            </ImageBackground>
        );
    }

}

const styles = StyleSheet.create({
    bg:{
        flex:1,
        width:null
    },
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    title:{
        fontSize:30,
        backgroundColor:'transparent',
        color:'#FFF',
        marginTop:10
    }
});