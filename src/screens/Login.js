import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableHighlight } from 'react-native';
import firebase from '../FirebaseConnection';

export default class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            emailInput:'',
            senhaInput:''
        };

        this.login = this.login.bind(this);

        firebase.auth().signOut();
    }

    login() {

        if(this.state.emailInput != '' && this.state.senhaInput != '') {

            firebase.auth().onAuthStateChanged((user)=>{
                if(user) {

                    this.props.navigation.navigate('Interna');

                }
            });

            firebase.auth().signInWithEmailAndPassword(
                this.state.emailInput, this.state.senhaInput
            ).catch((error)=>{
                alert(error.code);
            });
        }
    }

    render() {
        return(
            <View style={styles.container}>
                <Text>E-mail</Text>
                <TextInput style={styles.input} onChangeText={(emailInput)=>this.setState({emailInput})} />

                <Text>Senha</Text>
                <TextInput secureTextEntry={true} style={styles.input} onChangeText={(senhaInput)=>this.setState({senhaInput})} />
            
                
                <TouchableHighlight underlayColor="#CCC" style={styles.button} onPress={this.login}>
                    <Text style={styles.buttonTitle}>Login</Text>
                </TouchableHighlight>
                
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container:{
        margin:10,
    },
    input:{
        backgroundColor:'#FFF',
        height:40,
        padding:5,
        marginBottom:10,
        borderRadius:5,
        borderWidth:0.3,
        borderColor:'#CCC'
    },
    button:{
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#0B2161',
        height:40,
        width:100,
        borderRadius:5,
        marginTop:10
    },
    buttonTitle:{
        fontSize:15,
        color:'#FFF'
    }
});