import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableHighlight } from 'react-native';
import firebase from '../FirebaseConnection';

export default class Cadastro extends Component {

    constructor(props) {
        super(props);
        this.state = {
            nomeInput:'',
            emailInput:'',
            senhaInput:''
        };

        this.cadastrar = this.cadastrar.bind(this);

        firebase.auth().signOut();
    }

    cadastrar() {

        if(this.state.nomeInput != '' && this.state.emailInput != '' && this.state.senhaInput != '') {

            firebase.auth().onAuthStateChanged((user)=>{
                if(user) {

                    let uid = user.uid;
                    firebase.database().ref('users').child(uid).set({
                        saldo:0,
                        nome:this.state.nomeInput
                    });
                    this.props.navigation.navigate('Interna');

                }
            });

            firebase.auth().createUserWithEmailAndPassword(
                this.state.emailInput, this.state.senhaInput
            ).catch((error)=>{
                alert(error.code);
            });

        }

    }

    render() {
        return(
            <View style={styles.container}>
                <Text>Nome</Text>
                <TextInput style={styles.input} onChangeText={(nomeInput)=>this.setState({nomeInput})} />

                <Text>E-mail</Text>
                <TextInput style={styles.input} onChangeText={(emailInput)=>this.setState({emailInput})} />

                <Text>Senha</Text>
                <TextInput secureTextEntry={true} style={styles.input} onChangeText={(senhaInput)=>this.setState({senhaInput})} />
            
                
                <TouchableHighlight underlayColor="#CCC" style={styles.button} onPress={this.cadastrar}>
                    <Text style={styles.buttonTitle}>Cadastrar</Text>
                </TouchableHighlight>
                
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container:{
        margin:10,
    },
    input:{
        backgroundColor:'#FFF',
        height:40,
        padding:5,
        marginBottom:10,
        borderRadius:5,
        borderWidth:0.3,
        borderColor:'#CCC'
    },
    button:{
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#0B2161',
        height:40,
        width:100,
        borderRadius:5,
        marginTop:10
    },
    buttonTitle:{
        fontSize:15,
        color:'#FFF'
    }
});