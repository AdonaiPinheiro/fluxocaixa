import firebase from 'firebase';

let config = {
    apiKey: "AIzaSyC7ck9hmhT-vcvgCG2q9UFD2ItvSjpIh6Q",
    authDomain: "fluxo-caixa-e1753.firebaseapp.com",
    databaseURL: "https://fluxo-caixa-e1753.firebaseio.com",
    projectId: "fluxo-caixa-e1753",
    storageBucket: "",
    messagingSenderId: "772503709588"
}

firebase.initializeApp(config);

export default firebase;