import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, Button } from 'react-native';
import firebase from './FirebaseConnection';

export default class AddDespesas extends Component {

    constructor(props) {
        super(props);
        this.state = {
            valor:''
        };

        this.add = this.add.bind(this);
    }

    add() {
        if(this.state.valor != '') {

            let uid = firebase.auth().currentUser.uid;

            let key = firebase.database().ref('historico').child(uid).push().key;

            let user = firebase.database().ref('users').child(uid);

            firebase.database().ref('historico').child(uid).child(key).set({
                type:'despesa',
                valor:this.state.valor
            });

            user.once('value', (snapshot)=>{
                let saldo = parseFloat(snapshot.val().saldo);
                saldo -= parseFloat(this.state.valor);

                user.set({
                    saldo:saldo
                });

                this.props.navigation.goBack();
            });

        }
    }

    render() {
        return(
            <View style={styles.container}>
                <Text>Quanto você quer retirar</Text>
                <TextInput
                    style={styles.input}
                    keyboardType='numeric'
                    value={this.state.valor}
                    onChangeText={(valor)=>this.setState({valor})}
                    autoFocus={true}
                />
                <Button title="Retirar" onPress={this.add} />
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container:{
        flex:1,
        margin:10
    },
    input:{
        height:40,
        backgroundColor:'#FFF',
        borderWidth:0.3,
        borderRadius:5,
        padding:10
    }
});