import React, { Component } from 'react';
import { createAppContainer, createStackNavigator } from 'react-navigation';

//Screens
import Home from './src/screens/Home';
import Cadastro from './src/screens/Cadastro';
import Login from './src/screens/Login';
import Interna from './src/screens/Interna';
import Preload from './src/screens/Preload';
import AddReceita from './src/AddReceita';
import AddDespesas from './src/AddDespesas';


const Navigation = createStackNavigator({
  Preload:{
    screen:Preload,
    navigationOptions:{
      title:null,
      header:null
    }
  },
  Home:{
    screen:Home,
    navigationOptions:{
      title:null,
      header:null
    }
  },
  Cadastro:{
    screen:Cadastro,
    navigationOptions:{
      title:'Cadastro'
    }
  },
  Login:{
    screen:Login,
    navigationOptions:{
      title:'Login'
    }
  },
  Interna:{
    screen:Interna,
    navigationOptions:{
      title:'Home',
      headerLeft:null
    }
  },
  AddReceita:{
    screen:AddReceita,
    navigationOptions:{
      title:'Adicionar Receita'
    }
  },
  AddDespesas:{
    screen:AddDespesas,
    navigationOptions:{
      title:'Adicionar Despesa'
    }
  }
}, {
  defaultNavigationOptions:{
    headerStyle:{
      backgroundColor:'#0B2161',
    },
    headerTintColor:'#FFF'
  }
});

export default createAppContainer(Navigation);